import csv
import json


def prep_cwd(output_path, manifest_name):
    if os.path.exists(output_path):
        print("specified output folder already exists. Overwriting...")
        os.system("rm -r " + output_path)
    os.mkdir(output_path)


def make_s3_file_path(s3_path, output_file_name):
    return s3_path + "/" + output_file_name + ".gz"


def split(filename,
          s3_path,
          delimiter=',',
          row_limit=10000,
          output_dir_name='output',
          keep_headers=True,
          cur_name="cost2"):

    output_path = "./" + output_dir_name
    output_name_template = cur_name + '-%s.csv'
    manifest_name = cur_name + "-Manifest.json"

    prep_cwd(output_path, manifest_name)
    reader = csv.reader(open(filename, 'r'), delimiter=delimiter)
    current_piece = 1
    current_out_path = os.path.join(
         output_path,
         output_name_template % current_piece
    )
    outfile = open(current_out_path, 'w')
    current_out_writer = csv.writer(outfile, delimiter=delimiter)
    current_limit = row_limit
    if keep_headers:
        headers = next(reader)
        current_out_writer.writerow(headers)
    filenames = [make_s3_file_path(s3_path, output_name_template % 1)]
    total_lines = 0

    for i, row in enumerate(reader):
        if i + 1 > current_limit:
            current_piece += 1
            current_limit = row_limit * current_piece
            output_file_name = output_name_template % current_piece
            current_out_path = os.path.join(
               output_path,
               output_file_name
            )
            filenames.append(make_s3_file_path(s3_path, output_file_name))
            outfile.close()
            outfile = open(current_out_path, 'w')
            current_out_writer = csv.writer(outfile, delimiter=delimiter)
            if keep_headers:
                current_out_writer.writerow(headers)
        current_out_writer.writerow(row)
        total_lines = i + 1
    outfile.close()
    os.system("gzip " + output_path + "/*.csv")
    manifest = {"contentType": "text/csv",
                "reportKeys":  filenames}

    with open(output_path + "/" + manifest_name, 'w') as output_json:
        json.dump(manifest, output_json)
    print("total lines: " + str(total_lines))
