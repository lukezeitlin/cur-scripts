SELECT
line_item_usage_account_id AS HoldingAccountId,
LTRIM(line_item_product_code, 'Amazon') AS service_type,
reservation_reservation_a_r_n AS reserved_instances_id,
'' AS offering_id,
product_region AS region,
line_item_availability_zone AS availability_zone,
from_iso8601_timestamp(reservation_start_time) AS start_time,
from_iso8601_timestamp(reservation_end_time) AS end_time,
date_diff('second',
          from_iso8601_timestamp(reservation_start_time),
          from_iso8601_timestamp(reservation_end_time)) AS duration,
CASE
  WHEN strpos(line_item_usage_type,':') = 0 THEN 'm1.small'
  ELSE substr(line_item_usage_type, strpos(line_item_usage_type,':') + 1)
END AS instance,
reservation_upfront_value AS fixed_price,
'0.0' AS usage_price,
line_item_currency_code AS currency_code,
reservation_number_of_reservations AS instance_count,
CASE
  WHEN line_item_operation = 'RunInstances'            THEN 'Linux/UNIX'
  WHEN line_item_operation = 'RunInstances:0002'       THEN 'Windows'
  WHEN line_item_operation = 'RunInstances:0006'       THEN 'Windows with SQL Server Standard'
  WHEN line_item_operation = 'RunInstances:0010'       THEN 'Red Hat Enterprise Linux'
  WHEN line_item_operation = 'CreateCacheCluster:0002' THEN 'Redis'
  WHEN line_item_operation = 'CreateDBInstance:0002'   THEN 'MySQL'
  WHEN line_item_operation = 'CreateDBInstance:0011'   THEN 'SQL Server Web(LI)'
  WHEN line_item_operation = 'CreateDBInstance:0012'   THEN 'SQL Server SE (LI)'
  WHEN line_item_operation = 'CreateDBInstance:0014'   THEN 'PostgreSQL'
  WHEN line_item_operation = 'CreateDBInstance:0016'   THEN 'Aurora'
  WHEN line_item_operation = 'CreateDBInstance:0018'   THEN 'MariaDB'
  WHEN line_item_operation = 'RunComputeNode:0001'     THEN 'Redshift'
ct_description,
CASE
  WHEN reservation_end_time <> ''
   AND date_diff('second',
          now(),
          from_iso8601_timestamp(reservation_end_time)) <= 0 THEN 'retired'
  ELSE 'active'
END AS state,
CASE -- TODO cast to numbers for equality check
  WHEN reservation_upfront_value = 0.0 AND line_item_unblended_rate <> '0.0000000000' THEN 'No Upfront'
  WHEN reservation_upfront_value <> 0.0 AND line_item_unblended_rate <> '0.0000000000' THEN 'Partial Upfront'
  WHEN reservation_upfront_value <> 0.0 AND line_item_unblended_rate = '0.0000000000' THEN 'All Upfront'
END AS offering_type,
CASE
  WHEN line_item_product_code = 'AmazonRDS' THEN 'TODO'
  ELSE null
END AS multi_availability_zone,
nullif('','') AS tags,
nullif('','') AS instance_tenancy,
line_item_unblended_rate AS recurring_charges,
nullif('','') AS offering_class,
CASE
  WHEN line_item_availability_zone = '' THEN 'Region'
  ELSE 'Availability Zone'
END AS scope
FROM dazn
WHERE line_item_line_item_type='RIFee'
