SELECT bill_invoice_id AS bill_InvoiceId,
         bill_payer_account_id AS bill_PayerAccountId ,
         line_item_usage_account_id AS lineItem_UsageAccountId,
         line_item_line_item_type AS lineItem_LineItemType ,
         product_product_name AS product_ProductName ,
         date_format(line_item_usage_start_date, '%Y-%m-%dT%TZ') AS lineItem_UsageStartDate,
         date_format(line_item_usage_end_date, '%Y-%m-%dT%TZ') AS lineItem_UsageEndDate, 
         line_item_usage_type AS lineItem_UsageType,
         line_item_operation AS lineItem_Operation , 
         line_item_availability_zone AS lineItem_AvailabilityZone, 
         line_item_unblended_rate AS lineItem_UnBlendedRate , 
         line_item_blended_rate AS lineItem_BlendedRate , 
         line_item_usage_amount AS lineItem_UsageAmount, 
         line_item_line_item_description AS lineItem_LineItemDescription, 
         product_instance_type AS product_instanceType, 
         line_item_blended_cost AS lineItem_BlendedCost, 
         line_item_blended_cost AS lineItem_UnBlendedCost
FROM <TABLE_NAME>
where month = '<SOME_MONTH>'
